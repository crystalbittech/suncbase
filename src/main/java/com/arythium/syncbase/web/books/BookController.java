package com.arythium.syncbase.web.books;



import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.application.bankfiles.service.BookService;
import com.arythium.syncbase.application.fileobjects.dto.BookObjectDTO;
import com.arythium.syncbase.application.fileobjects.service.BookObjectService;
import com.arythium.syncbase.core.exceptions.SyncBaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/books")
public class BookController {

    private static final Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    BookService  bookService;

    @Autowired
    BookObjectService bookObjectService;

    /***
     * ############# Books
     * */

    @GetMapping("")
    public String index(Model model ){
        List<BankBookDTO> bankBookDTOS = bookService.getAllBooks();
        model.addAttribute("books",bankBookDTOS);
        return "books/list";
    }

    @GetMapping("/add")
    public String addBookView(Model model){
        model.addAttribute("book",new BankBookDTO());
        return "books/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addBook(@Valid @ModelAttribute("book") BankBookDTO bankBookDTO , BindingResult result, RedirectAttributes redirectAttributes, Model model, Locale locale){

        logger.info("BankBook details: {}", bankBookDTO);
        try{
            bankBookDTO = bookService.addBook(bankBookDTO);
            model.addAttribute("successMessage", "Successfully added Book");
            model.addAttribute("book",new BankBookDTO());
        }catch(SyncBaseException e){
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
        }
        return "books/add";
    }

    @RequestMapping(value = "/bulk/upload", method = RequestMethod.POST)
    public String uploadBooks(@RequestParam("uploadFile") MultipartFile file , Model model, RedirectAttributes redirectAttributes){

        if (file.isEmpty()) {
            logger.info("Empty file");
            redirectAttributes.addFlashAttribute("errorMessage", "Please select a file to upload");
            return "redirect:/books/add";
        }
        try{
            bookService.uploadBooks(file);
            model.addAttribute("successMessage", "Successfully Uploaded Books");
        }catch(SyncBaseException e){
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
        }
        List<BankBookDTO> bankBookDTOS = bookService.getAllBooks();
        model.addAttribute("books",bankBookDTOS);
        return "books/list";
    }



    @GetMapping("/{id}")
    public String getBookView(@PathVariable("id") Long id , Model model){

        BankBookDTO bankBookDTO = bookService.getBookById(id);
        model.addAttribute("book",bankBookDTO);
        return "books/edit";
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public String editBook(@PathVariable("id") Long id , @Valid @ModelAttribute("book") BankBookDTO bankBookDTO , Model model){

        logger.info("BankBook details: {}", bankBookDTO);
        try{
            bankBookDTO = bookService.updateBook(bankBookDTO);
            model.addAttribute("successMessage", "Successfully Updated Book");
        }catch(SyncBaseException e){
            model.addAttribute("errorMessage", e.getMessage());
            bankBookDTO = bookService.getBookById(id);
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
            bankBookDTO = bookService.getBookById(id);
        }

        model.addAttribute("book",bankBookDTO);
        return "books/edit";
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public String deleteBook(@PathVariable("id") Long id , Model model, Locale locale){

        try{
            bookService.deleteBook(id);
            model.addAttribute("successMessage", "Successfully Deleted Book");
        }catch(SyncBaseException e){
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
        }
        List<BankBookDTO> bankBookDTOS = bookService.getAllBooks();
        model.addAttribute("books",bankBookDTOS);
        return "books/list";
    }


    /***
     * ############# Books Objects
     * */

    @GetMapping("/{bankBookId}/objects")
    public String getBookObjectsView(@PathVariable("bankBookId") Long bankBookId , Model model, RedirectAttributes redirectAttributes){

        List<BookObjectDTO> feedObjectDTOList = bookObjectService.getFeedsByBookId(bankBookId);
        model.addAttribute("bookobjects",feedObjectDTOList);
        model.addAttribute("bookId",bankBookId);
        return "bookobjects/list";
    }



    @GetMapping("/{bankBookId}/objects/add")
    public String addBookObjectsView(@PathVariable("bankBookId") Long bankBookId ,Model model, RedirectAttributes redirectAttributes){

        model.addAttribute("bookobject",new BookObjectDTO());
        model.addAttribute("bookId",bankBookId);
        return "bookobjects/add";
    }

    @RequestMapping(value = "/{bankBookId}/objects/add", method = RequestMethod.POST)
    public String addBookObject(@PathVariable("bankBookId") Long bankBookId ,@Valid @ModelAttribute("bookobject") BookObjectDTO bookObjectDTO , BindingResult result, RedirectAttributes redirectAttributes, Model model, Locale locale){

        logger.info("bookObjectDTO details: {}", bookObjectDTO);

        try{
            BankBookDTO   bankBookDTO = bookService.getBookById(bankBookId);
            bookObjectDTO.setBankBookDTO(bankBookDTO);
            bookObjectService.addBookObject(bookObjectDTO);
            List<BookObjectDTO> feedObjectDTOList = bookObjectService.getFeedsByBookId(bankBookId);
            model.addAttribute("bookobjects",feedObjectDTOList);
            model.addAttribute("bookId",bankBookId);
            model.addAttribute("successMessage", "Successfully Added Object");
            return "bookobjects/list";
        }catch (SyncBaseException e){
            e.printStackTrace();
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            model.addAttribute("errorMessage", e.getMessage());
        }

        model.addAttribute("bookobject",bookObjectDTO);
        model.addAttribute("bookId",bankBookId);
        return bankBookId+"bookobjects/add";
    }

    @RequestMapping(value = "/{bookId}/objects/bulk/upload", method = RequestMethod.POST)
    public String uploadBookObjects(@PathVariable("bookId") Long bookId,@RequestParam("uploadFile") MultipartFile file,
                                    Model model, RedirectAttributes redirectAttributes){

        if (file.isEmpty()) {
            logger.info("Empty Object file");
            model.addAttribute("errorMessage", "Please select a file to upload");
        }else{
            try{
                bookObjectService.uploadBookObjects(file,bookId);
                model.addAttribute("successMessage", "Successfully Uploaded Books");
            }catch(SyncBaseException e){
                model.addAttribute("errorMessage", e.getMessage());
            }catch (Exception e){
                model.addAttribute("errorMessage", e.getMessage());
            }
        }

        List<BookObjectDTO> feedObjectDTOList = bookObjectService.getFeedsByBookId((bookId));
        model.addAttribute("bookobjects",feedObjectDTOList);
        model.addAttribute("bookId",bookId);
        return "bookobjects/list";
    }


    @GetMapping("/{bankBookId}/objects/{bookObjectId}")
    public String getBookObjectView(@PathVariable("bankBookId") Long bankBookId ,@PathVariable("bookObjectId") Long bookObjectId , Model model, RedirectAttributes redirectAttributes){

        BookObjectDTO feedObjectDTO = bookObjectService.getBookObjectById(bookObjectId);
        model.addAttribute("bookobject",feedObjectDTO);
        model.addAttribute("bookId",bankBookId);
        return "bookobjects/edit";
    }

    @RequestMapping(value = "/{bankBookId}/objects/{bookObjectId}", method = RequestMethod.POST)
    public String editBookObject(@PathVariable("bankBookId") Long bankBookId ,@PathVariable("bookObjectId") Long bookObjectId ,@Valid @ModelAttribute("bookobject") BookObjectDTO bookObjectDTO , BindingResult result, RedirectAttributes redirectAttributes, Model model, Locale locale){

        logger.info("bookObjectDTO details: {}", bookObjectDTO);

        try{
            BankBookDTO   bankBookDTO = bookService.getBookById(bankBookId);
            if(bankBookDTO == null){
                throw new SyncBaseException("Book Not Found :: "+bankBookId);
            }
            bookObjectDTO.setBankBookDTO(bankBookDTO);
            bookObjectService.updateBookObject(bookObjectDTO);
            model.addAttribute("successMessage", "Successfully Updated Object");
        }catch (SyncBaseException e){
            e.printStackTrace();
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            model.addAttribute("errorMessage", e.getMessage());
        }

        BookObjectDTO bookObjectDTO1 = bookObjectService.getBookObjectById(bookObjectId);
        model.addAttribute("bookobjects",bookObjectDTO1);
        model.addAttribute("bookId",bankBookId);
        return "bookobjects/edit";
    }

    @RequestMapping(value = "/{bankBookId}/objects/{bookObjectId}/delete", method = RequestMethod.GET)
    public String deleteBookObject(@PathVariable("bankBookId") Long bankBookId ,@PathVariable("bookObjectId") Long bookObjectId ,Model model, Locale locale){

        try{
            bookObjectService.deleteBookObject(bookObjectId);
            model.addAttribute("successMessage", "Successfully Deleted Object");
        }catch (SyncBaseException e){
            e.printStackTrace();
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            model.addAttribute("errorMessage", e.getMessage());
        }

        List<BookObjectDTO> feedObjectDTOList = bookObjectService.getFeedsByBookId(bankBookId);
        model.addAttribute("bookobjects",feedObjectDTOList);
        model.addAttribute("bookId",bankBookId);
        return "bookobjects/list";
    }







}
