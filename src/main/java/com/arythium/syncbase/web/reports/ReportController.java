package com.arythium.syncbase.web.reports;


import com.arythium.syncbase.application.filesync.dto.BookSyncDto;
import com.arythium.syncbase.application.filesync.entity.BookSyncAudit;
import com.arythium.syncbase.application.filesync.repository.BookSyncAuditRepository;
import com.arythium.syncbase.application.filesync.service.BookSyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/reports")
public class ReportController {

    private static final Logger logger = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    private BookSyncService bookSyncService;

    @Autowired
    private BookSyncAuditRepository bookSyncAuditRepository;


    @GetMapping("/book_sync")
    public String index(Model model){

        List<BookSyncDto> bookSyncDtoList = bookSyncService.getSyncAudit();
        model.addAttribute("syncAudits", bookSyncDtoList);
        return "reports/book_sync/list";
    }


    @GetMapping("/book_sync/{procID}/edit")
    public String viewBookSyncAudit(@PathVariable("procID") String procID , Model model){

        BookSyncDto bookSyncDto = bookSyncService.getBookSyncByProcID(procID);
        List<BookSyncAudit> bookSyncAudits = bookSyncAuditRepository.findByBookSync_Id(bookSyncDto.getId());
//        System.out.println("returnnng :: "+bookSyncAudits);
        model.addAttribute("book_sync",bookSyncDto);
        model.addAttribute("book_sync_audits",bookSyncAudits);
        return "reports/book_sync/edit";
    }


}
