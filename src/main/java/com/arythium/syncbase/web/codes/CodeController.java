package com.arythium.syncbase.web.codes;


import com.arythium.syncbase.application.filesync.dto.BookSyncDto;
import com.arythium.syncbase.application.filesync.service.BookSyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/codes")
public class CodeController {

    private static final Logger logger = LoggerFactory.getLogger(CodeController.class);

//    private final MessageSource messageSource;
    @Autowired
    private BookSyncService bookSyncService;


    @GetMapping("")
    public String index(Model model){

        List<BookSyncDto> bookSyncDtoList = bookSyncService.getSyncAudit();
        model.addAttribute("syncAudits", bookSyncDtoList);
        return "codes/list";
    }

    @GetMapping("/add")
    public String addPage(){

        return "codes/add";
    }

    @GetMapping("/{id}/edit")
    public String editPage(){

        return "codes/edit";
    }




}
