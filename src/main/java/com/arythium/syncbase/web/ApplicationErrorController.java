package com.arythium.syncbase.web;//package com._3line.gravity.web;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;


@Controller
public class ApplicationErrorController implements ErrorController {

    private static final String PATH = "/error";

    @Autowired
    private ErrorAttributes errorAttributes;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Value("${dev.members.mail}")
    private String devMembersMails;


    @RequestMapping(value = PATH)
    public String handleError(WebRequest request) {

        Map<String, Object> errorDetails = errorAttributes.getErrorAttributes(request, true);

        String errorPath = (String) errorDetails.get("path");
        String statusCode = errorDetails.get("status").toString();
        Object exception = errorDetails.get("exception");

        logger.error("Page Error Detail: {}",errorDetails.get("error"));


        if (exception != null) {
            sendNotification(errorDetails);
        }

        if("403".equals(statusCode)){
            return "error403";
        }

        if("404".equals(statusCode)){
            return "error404";
        }

        String subPath = StringUtils.substringAfter(errorPath, "/");


        if (subPath != null) {
            return "error";
        }

        return "redirect:/login";
    }


    @Override
    public String getErrorPath() {
        return PATH;
    }


    private void sendNotification(Map errorDetails) {

    }
}
