package com.arythium.syncbase.web.dashboard;


import com.arythium.syncbase.application.filesync.dto.BookSyncDashBoardAudit;
import com.arythium.syncbase.application.filesync.service.BookSyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class DashBoardController {

    private static final Logger logger = LoggerFactory.getLogger(DashBoardController.class);

    @Autowired
    BookSyncService bookSyncService;

    @RequestMapping("/")
    public String index(){

        return "redirect:/dashboard";
    }

    @GetMapping("/dashboard")
    public String dashBoardIndex(Model model){

        BookSyncDashBoardAudit bookSyncDashBoardAudit = bookSyncService.getSyncDashBoardData();
        model.addAttribute("dashBoardData", bookSyncDashBoardAudit);
        return "dashboard/index";
    }



//     @GetMapping("/aggregator")
//     @PreAuthorize("hasAuthority('VIEW_AGENTS')")
//    public String aggregators(){ return "agents/aggregators"; }
//
//    @GetMapping(value = "/{id}/")
//    public  String agent(@PathVariable("id") Long id , Model model){
//        viewAgent(model , id);
//        return "agents/view-agent";
//    }



}
