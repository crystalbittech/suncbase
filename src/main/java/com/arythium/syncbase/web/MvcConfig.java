//package com.arythium.syncbase.web;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
//import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//import org.springframework.web.servlet.view.InternalResourceViewResolver;
//import org.springframework.web.servlet.view.JstlView;
//
//@Configuration
//public class MvcConfig implements WebMvcConfigurer {
//
//	public void addViewControllers(ViewControllerRegistry registry) {
//		registry.addViewController("/home").setViewName("/dashboard/index.html");
//		registry.addViewController("/").setViewName("/home.html");
//		registry.addViewController("/hello").setViewName("/hello.html");
//		registry.addViewController("/login").setViewName("/login.html");
//	}
//
//	@Override
//	public void configureViewResolvers(ViewResolverRegistry registry) {
//		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//		resolver.setPrefix("/templates/");
////		resolver.setSuffix(".html");
//		resolver.setViewClass(JstlView.class);
//		registry.viewResolver(resolver);
//	}
//
//}
