package com.arythium.syncbase.web.settings;


import com.arythium.syncbase.core.exceptions.SyncBaseException;
import com.arythium.syncbase.core.setting.dto.SettingDto;
import com.arythium.syncbase.core.setting.service.SettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/settings")
public class SettingController {

    private static final Logger logger = LoggerFactory.getLogger(SettingController.class);

    @Autowired
    private SettingService settingService;


    @GetMapping("")
    public String index(Model model){

        List<SettingDto> allSettings = settingService.getSettings();
        model.addAttribute("settings",allSettings);
        return "settings/list";
    }

    @GetMapping("/add")
    public String addSettingPage(Model model){

        model.addAttribute("setting",new SettingDto());
        return "settings/add";
    }

    @PostMapping("/add")
    public String addSetting(@Valid @ModelAttribute("setting") SettingDto SettingDto , Model model){

        try{
            settingService.addSetting(SettingDto);
            model.addAttribute("successMessage", "Successfully Created Setting");
        }catch(SyncBaseException e){
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
        }

        List<SettingDto> allSettings = settingService.getSettings();
        model.addAttribute("settings",allSettings);
        return "settings/list";
    }

    @GetMapping("/{id}/edit")
    public String editPage(@PathVariable("id") Long id , Model model){

        SettingDto settingDto = settingService.getSettingById(id);
        model.addAttribute("setting",settingDto);
        return "settings/edit";
    }

    @PostMapping("/{id}/edit")
    public String editPage(@PathVariable("id") Long id , @Valid @ModelAttribute("setting") SettingDto SettingDto , Model model){

        try{
            settingService.updateSetting(SettingDto);
            model.addAttribute("successMessage", "Successfully Updated Setting");
        }catch(SyncBaseException e){
            model.addAttribute("errorMessage", e.getMessage());
        }catch (Exception e){
            model.addAttribute("errorMessage", e.getMessage());
        }
        model.addAttribute("setting",SettingDto);
        return "settings/edit";
    }




}
