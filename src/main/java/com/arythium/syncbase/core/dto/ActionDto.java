package com.arythium.syncbase.core.dto;


import lombok.Data;
import lombok.ToString;

@Data
public class ActionDto extends AbstractDto {

    private boolean enabled;

    private String code ;

    private String description ;
}
