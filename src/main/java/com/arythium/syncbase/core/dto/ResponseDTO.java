/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arythium.syncbase.core.dto;

import lombok.Data;

/**
 * @author TimothyO
 */
@Data
public class ResponseDTO {

    private String respCode;
    private String respDescription;
    private Object respBody;
}
