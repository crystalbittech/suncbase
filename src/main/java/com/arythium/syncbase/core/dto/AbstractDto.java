package com.arythium.syncbase.core.dto;

import lombok.Data;

import java.util.Date;

@Data
public abstract class AbstractDto {

    private Long id ;
    private int version;
    private Date dateCreated;
    private Date dateDeleted;
    private String delFlag;
}
