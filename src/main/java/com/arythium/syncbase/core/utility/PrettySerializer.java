package com.arythium.syncbase.core.utility;

import com.fasterxml.jackson.databind.JsonSerializer;

public interface PrettySerializer
{
    public  <T> JsonSerializer<T> getSerializer();
}
