package com.arythium.syncbase.core.security.service;

import com.arythium.syncbase.core.usermgt.model.SystemUser;
import com.arythium.syncbase.core.usermgt.repository.ApplicationUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author FortunatusE
 * @date 11/28/2018
 */

@Service
public class LoginService {

    private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

    @Autowired
    private ApplicationUserRepository userRepository;


    @Autowired
    private PasswordEncoder passwordEncoder;


    public void updateUserLoginAttempt(SystemUser user){

        int numOfLoginAttempts = user.getNoOfWrongLoginCount();
        user.setNoOfWrongLoginCount(++numOfLoginAttempts);
        userRepository.save(user);
        logger.info("Updated failed login count for user [{}]", user.getUserName());

    }


    public void updateLastLogin(SystemUser user){

        user.setLastLoginDate(new Date());
        userRepository.save(user);
        logger.info("Updated last login date for user [{}]", user.getUserName());
    }


}
