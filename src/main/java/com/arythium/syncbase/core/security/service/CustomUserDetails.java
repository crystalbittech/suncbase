package com.arythium.syncbase.core.security.service;


import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by FortunatusE on 9/22/2018.
 */
public interface CustomUserDetails extends UserDetails {
}
