package com.arythium.syncbase.core.usermgt.model;

import com.arythium.syncbase.core.code.model.Code;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@ToString
@Entity(name = "app_system_user")
@Where(clause ="del_Flag='N'" )
public class SystemUser extends AbstractUser {


}
