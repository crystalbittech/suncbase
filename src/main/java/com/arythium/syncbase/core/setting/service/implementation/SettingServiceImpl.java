package com.arythium.syncbase.core.setting.service.implementation;

import com.arythium.syncbase.core.exceptions.SyncBaseException;
import com.arythium.syncbase.core.setting.dto.SettingDto;
import com.arythium.syncbase.core.setting.model.AppSetting;
import com.arythium.syncbase.core.setting.repository.AppSettingRepository;
import com.arythium.syncbase.core.setting.service.SettingService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * Created by FortunatusE on 8/3/2018.
 */

@Service
public class SettingServiceImpl implements SettingService {

    private static final Logger logger = LoggerFactory.getLogger(SettingServiceImpl.class);

    private AppSettingRepository appSettingRepository;
    private MessageSource messageSource;
    private ModelMapper modelMapper;
    private final Locale locale = LocaleContextHolder.getLocale();


    @Autowired
    public SettingServiceImpl(AppSettingRepository appSettingRepository, MessageSource messageSource, ModelMapper modelMapper) {
        this.appSettingRepository = appSettingRepository;
        this.messageSource = messageSource;
        this.modelMapper = modelMapper;
    }

    @Override
    public String addSetting(SettingDto settingDto) {

        logger.debug("Adding setting {}", settingDto);

        validateNonExistingSetting(settingDto);
        AppSetting setting = convertDtoToEntity(settingDto);
        setting.setDelFlag("N");

        try {
            AppSetting newSetting = appSettingRepository.save(setting);
            logger.info("Added setting {}", newSetting);
            return messageSource.getMessage("setting.add.success", null, locale);
        } catch (Exception e) {
            logger.error("Failed to add setting {}", settingDto, e);
            throw new SyncBaseException(messageSource.getMessage("setting.add.failure", null, locale), e);
        }

    }

//    @RequireApproval(code = "UPDATE_SETTING", entityType = AppSetting.class)
    @Override
    public String updateSetting(SettingDto settingDto) {

        logger.info("Updating setting {}", settingDto);

        AppSetting appSetting = appSettingRepository.findById(settingDto.getId()).orElse(null);

        if(appSetting==null){
            throw new SyncBaseException("Setting Not Found");
        }

        appSetting.setName(settingDto.getName());
        appSetting.setDescription(settingDto.getDescription());
        appSetting.setValue(settingDto.getValue());
        appSetting.setCode(settingDto.getCode());

        appSettingRepository.save(appSetting);

        return messageSource.getMessage("setting.update.success", null, locale);

    }


    private void validateNonExistingSetting(SettingDto settingDto) {

        AppSetting setting = appSettingRepository.findByCode(settingDto.getCode());

        if (setting != null && !setting.getId().equals(settingDto.getId())) {
            throw new SyncBaseException(messageSource.getMessage("setting.exists", null, locale));
        }
    }

    @Override
    public SettingDto getSettingById(Long id) {

        logger.debug("Getting setting by Id [{}]", id);

        AppSetting setting = appSettingRepository.findById(id).orElse(null);
        return convertEntityToDto(setting);
    }


    @Override
    public String deleteSetting(Long id) {

        logger.debug("Deleting setting with Id {}", id);

        AppSetting setting = appSettingRepository.findById(id).orElse(null);

        try {
            appSettingRepository.delete(setting);
            logger.warn("Deleted setting {}", setting);
            return messageSource.getMessage("setting.delete.success", null, locale);
        } catch (Exception e) {
            logger.error("Failed to delete setting {}", setting, e);
            throw new SyncBaseException(messageSource.getMessage("setting.delete.failure", null, locale), e);
        }
    }


    @Override
    public boolean isSettingAvailable(String code) {
        boolean check = false;
        logger.info("checking ->{} configuration", code);
        AppSetting setting = appSettingRepository.findByCode(code);
        if (Objects.nonNull(setting)) {
            check = setting.isEnabled();
            logger.debug("AppSetting [{}] enabled status: {}", check);
        }
        return check;
    }

    @Override
    public List<SettingDto> getSettings() {
        List<AppSetting> settings = appSettingRepository.findAll();
        List<SettingDto> SettingDtos = convertEntitiesToDtos(settings);
        return SettingDtos;
    }


    private SettingDto convertEntityToDto(AppSetting setting) {

        SettingDto settingDto = modelMapper.map(setting, SettingDto.class);
        return settingDto;
    }

    private AppSetting convertDtoToEntity(SettingDto settingDto) {

        AppSetting setting = modelMapper.map(settingDto, AppSetting.class);
        return setting;
    }


    private List<SettingDto> convertEntitiesToDtos(List<AppSetting> settings) {

        return settings.stream().map(setting -> convertEntityToDto(setting)).collect(Collectors.toList());
    }
}



