package com.arythium.syncbase.core.setting.repository;

//import com.arythium.syncbase.core.repository.AppCommonRepository;
import com.arythium.syncbase.core.setting.model.AppSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by FortunatusE on 8/3/2018.
 */

@Repository
public interface AppSettingRepository extends JpaRepository<AppSetting, Long> {

    AppSetting findByCode(String code);

    boolean existsByCode(String code);
}

