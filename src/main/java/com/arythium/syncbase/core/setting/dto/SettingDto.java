package com.arythium.syncbase.core.setting.dto;


import com.arythium.syncbase.core.dto.AbstractDto;
import com.arythium.syncbase.core.utility.PrettySerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.IOException;

/**
 * Created by TimothyO on 8/3/2018.
 */
@Data
public class SettingDto extends AbstractDto implements PrettySerializer {

    @NotEmpty(message = "Name is required")
    private String name;
    private String description;
    @NotEmpty(message = "Code is required")
    private String code;
    private String value;
    private boolean enabled;
    private String csrf;






    @Override
    @JsonIgnore
    public JsonSerializer<SettingDto> getSerializer() {
        return new JsonSerializer<SettingDto>() {
            @Override
            public void serialize(SettingDto setting, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {

                gen.writeStartObject();
                gen.writeStringField("Name", setting.name);
                gen.writeStringField("Description", setting.description);
                gen.writeStringField("Code", setting.code);
                gen.writeStringField("Value", setting.value);
                gen.writeStringField("Enabled", String.valueOf(setting.enabled));
                gen.writeEndObject();
            }
        };
    }

}
