package com.arythium.syncbase.core.setting.service;

import com.arythium.syncbase.core.setting.dto.SettingDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by FortunatusE on 8/3/2018.
 */
public interface SettingService {

    /**
     * Adds the given system setting to the database
     * @param settingDto the setting to be added
     * @return success message
     */

    String addSetting(SettingDto settingDto);

    /**
     * Updates the given system setting
     * @param settingDto the setting to be updated
     * @return success message
     */

    String updateSetting(SettingDto settingDto);


    /**
     * Returns the setting specified by the given Id
     * @param id the setting's Id
     * @return the setting
     */

    SettingDto getSettingById(Long id);


    /**
     * Deletes the setting specified by the given Id
     * @param id the setting's Id
     * @return success message
     */

    String deleteSetting(Long id);

    /*"
     * Check if configuration is available and enabled
     * @param code the settings code
     * @return true if settings is available and enabled
     */
    boolean isSettingAvailable(String code);

    List<SettingDto> getSettings();

}
