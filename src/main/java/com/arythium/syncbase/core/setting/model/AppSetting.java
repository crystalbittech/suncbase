package com.arythium.syncbase.core.setting.model;

import com.arythium.syncbase.core.entity.AbstractEntity;
import com.arythium.syncbase.core.utility.PrettySerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by FortunatusE on 8/3/2018.
 */


@Entity
@Data
@Table(name = "app_setting")
@Where(clause ="del_Flag='N'" )
public class AppSetting extends AbstractEntity implements PrettySerializer {

    private String name;
    private String code;
    private String description;
    private String value;
    private boolean enabled;



    @JsonIgnore
    @Override
    public List<String> getDefaultSearchFields(){
        return Arrays.asList("name", "code");
    }

    @Override
    @JsonIgnore
    public JsonSerializer<AppSetting> getSerializer() {
        return new JsonSerializer<AppSetting>() {
            @Override
            public void serialize(AppSetting setting, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {

                gen.writeStartObject();
                gen.writeStringField("Name", setting.name);
                gen.writeStringField("Description", setting.description!=null?setting.description:"");
                gen.writeStringField("Code", setting.code);
                gen.writeStringField("Value", setting.value);
                gen.writeStringField("Enabled", String.valueOf(setting.enabled));
                gen.writeEndObject();
            }
        };
    }


}
