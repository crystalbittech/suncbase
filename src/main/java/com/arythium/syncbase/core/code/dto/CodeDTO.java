package com.arythium.syncbase.core.code.dto;


import com.arythium.syncbase.core.entity.AbstractEntity;
import com.arythium.syncbase.core.utility.PrettySerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.IOException;

/**
 * @author TimothyO
 * @date 11/6/2018
 */
@Data
public class CodeDTO extends AbstractEntity implements PrettySerializer {


    @NotEmpty(message = "code")
    private String code;

    @NotEmpty(message = "type")
    private String type;

    @NotEmpty(message = "description")
    private String description;

    private String extraInfo;
    private int version;



    @Override
    @JsonIgnore
    public JsonSerializer<CodeDTO> getSerializer() {
        return new JsonSerializer<CodeDTO>() {
            @Override
            public void serialize(CodeDTO code, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {

                gen.writeStartObject();
                gen.writeStringField("Code", code.code);
                gen.writeStringField("Type", code.type);
                gen.writeStringField("Description", code.description);
                gen.writeStringField("Extra Info", code.extraInfo);
                gen.writeEndObject();
            }
        };
    }

}

