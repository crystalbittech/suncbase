package com.arythium.syncbase.core.code.model;

import com.arythium.syncbase.core.entity.AbstractEntity;
import com.arythium.syncbase.core.utility.PrettySerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 *
 * The {@code Code} class model represents unique data that can be used for system configurations.
 * This can be used to set up a list of items that can be presented in drop-down menus
 * Example is a country eg Nigeria with code: NG, type: COUNTRY and description: the description if necessary
 * With this approach, new items can be added or removed from a list presented to the user
 * @author TimothyO
 * @date 11/6/2018
 */

@Data
@Entity
@Table(name="code", uniqueConstraints= @UniqueConstraint(columnNames={"code", "type"}))
@Where(clause ="del_Flag='N'" )
public class Code extends AbstractEntity implements PrettySerializer {

        private String code;
        private String type;
        private String description;

        @Column(name = "extra_info")
        private String extraInfo;



    @Override
    @JsonIgnore
    public JsonSerializer<Code> getSerializer() {
        return new JsonSerializer<Code>() {
            @Override
            public void serialize(Code code, JsonGenerator gen, SerializerProvider serializers)
                    throws IOException {

                gen.writeStartObject();
                gen.writeStringField("Code", code.code);
                gen.writeStringField("Type", code.type);
                gen.writeStringField("Description", code.description);
                gen.writeStringField("Extra Info", code.extraInfo);
                gen.writeEndObject();
            }
        };
    }


    @JsonIgnore
    @Override
    public List<String> getDefaultSearchFields(){
        return Arrays.asList("code", "type");
    }


        @Override
        public String toString() {
            return "Code{" +
                    "code='" + code + '\'' +
                    ", type='" + type + '\'' +
                    ", description='" + description + '\'' +
                    '}';
        }



}
