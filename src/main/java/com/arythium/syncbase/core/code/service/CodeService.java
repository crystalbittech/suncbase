package com.arythium.syncbase.core.code.service;


import com.arythium.syncbase.core.code.dto.CodeDTO;
import com.arythium.syncbase.core.code.model.Code;
import com.arythium.syncbase.core.code.model.CodeType;
import com.arythium.syncbase.core.exceptions.SyncBaseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * This {@code CodeService} interface provides the methods for managing system
 * codes. A {@code Code}can be used add new items to the system which can be
 * displayed in list or in drop down menus
 *

 */
public interface CodeService {

    String addCode(CodeDTO code) throws SyncBaseException;

    Page<CodeType> findByCodeType(String codeType, Pageable pageDetails);

    /**
     * Deletes a code from the system
     * @param codeId the oode's id
     */
    String deleteCode(Long codeId) throws SyncBaseException;

    /**
     * Returns the specified code
     *@param codeId the code's id
     *
     * @return the code
     */

    CodeDTO getCode(Long codeId);

    Code getCodeById(Long codeId);

    /**
     * Returns a list of codes specified by the given type
     *@param codeType the code's type
     * @return a list of codes
     */
    List<CodeDTO> getCodesByType(String codeType);


    public String updateCode(CodeDTO codeDTO) throws SyncBaseException;


    Page<CodeDTO> getCodesByType(String codeType, Pageable pageDetails);


    Page<CodeType> getCodeTypes(Pageable pageDetails);

    public Code getByTypeAndCode(String type, String code);


    Page<CodeDTO> findByTypeAndValue(String codeType, String value, Pageable pageDetails);


    Page<CodeDTO> getCodes(Pageable pageDetails);

    /**
     * Returns all the codes in the system
     *
     * @return a list of the codes
     */
    Iterable<CodeDTO> getCodes();

    CodeDTO convertEntityToDTO(Code code);

    Code convertDTOToEntity(CodeDTO codeDTO);

    List<CodeDTO> convertEntitiesToDTOs(Iterable<Code> codes);

    CodeDTO getCodeByCode(String code);

    CodeDTO getCodeByDescription(String description);

}