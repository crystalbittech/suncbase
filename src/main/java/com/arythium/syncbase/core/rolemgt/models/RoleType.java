package com.arythium.syncbase.core.rolemgt.models;

public enum RoleType {
    SYSTEM, BRANCH
}
