package com.arythium.syncbase.core.rolemgt.models;

public enum PermissionType {
    SYSTEM, BRANCH, BOTH, NONE;
}
