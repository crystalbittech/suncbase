package com.arythium.syncbase.core.rolemgt.repositories;


import com.arythium.syncbase.core.rolemgt.models.Role;
import com.arythium.syncbase.core.rolemgt.models.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findFirstByNameIgnoreCase(String s);

    Role findFirstByRoleType(RoleType roleType);

    Role findByNameAndRoleType(String name, RoleType type);

    List<Role> findByRoleType(RoleType type);


}
