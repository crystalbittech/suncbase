package com.arythium.syncbase.core.rolemgt.dtos;


import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Set;

@Data
public class RoleDTO{

    private Long id;
    private int version;
    @NotEmpty(message = "Name is required")
    private String name;
    private String description;
    @NotEmpty(message = "Type is required")
    private String type;
    private List<PermissionDTO> permissions;
    private Set<String> approvables;


}
