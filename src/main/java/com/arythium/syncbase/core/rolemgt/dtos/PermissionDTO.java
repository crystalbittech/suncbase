package com.arythium.syncbase.core.rolemgt.dtos;


import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class PermissionDTO {

    private Long id;
    private  int version;
    @NotEmpty
    private String name;
    private String description;
    private String type;

}
