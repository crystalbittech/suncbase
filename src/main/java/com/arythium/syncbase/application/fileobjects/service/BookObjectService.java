package com.arythium.syncbase.application.fileobjects.service;

import com.arythium.syncbase.application.fileobjects.dto.BookObjectDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BookObjectService {

    List<BookObjectDTO> getFeedsByBookName(String bookName);

    List<BookObjectDTO> getFeedsByBookId(Long bankBookId);

    List<BookObjectDTO> getPagedFeedsByBookId(Long bankBookId, Pageable pageable);

    List<BookObjectDTO> getAllFeeds();

    BookObjectDTO addBookObject(BookObjectDTO BookObjectDTO);

    BookObjectDTO getFeedByNameAndBookname(String name, String bookName);

    BookObjectDTO getBookObjectById(Long feedObjectId);

    BookObjectDTO updateBookObject(BookObjectDTO BookObjectDTO);

    String uploadBookObjects(MultipartFile file,Long bookId);

    void deleteBookObject(Long id);


}
