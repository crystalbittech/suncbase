package com.arythium.syncbase.application.fileobjects.service.implementation;

import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.application.bankfiles.service.BookService;
import com.arythium.syncbase.application.fileobjects.dto.BookObjectDTO;
import com.arythium.syncbase.application.fileobjects.entity.BookObject;
//import com.arythium.syncbase.application.feeds.repository.BookObjectEntityManager;
import com.arythium.syncbase.application.fileobjects.repository.BookObjectRepository;
import com.arythium.syncbase.application.fileobjects.service.BookObjectService;
import com.arythium.syncbase.core.exceptions.SyncBaseException;
import com.opencsv.CSVReader;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class BookObjectServiceImpl implements BookObjectService {

    private static final Logger logger = LoggerFactory.getLogger(BookObjectServiceImpl.class);

    @Value("${book_objects.config.header_columns}")
    private String[] configureHeaders;

    @Autowired
    BookObjectRepository bookObjectRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    BookService bookService;

    @Override
    public List<BookObjectDTO> getFeedsByBookName(String bookName) {
        List<BookObjectDTO> BookObjectDTOS  = new ArrayList<>();
        List<BookObject> feedObjects = bookObjectRepository.findByBankBook_BookName(bookName);
        for (BookObject feedObject : feedObjects) {
            BookObjectDTO BookObjectDTO = modelMapper.map(feedObject,BookObjectDTO.class);
            BookObjectDTOS.add(BookObjectDTO);
        }
        return BookObjectDTOS;
    }

    @Override
    public List<BookObjectDTO> getFeedsByBookId(Long bankBookId) {
        List<BookObjectDTO> BookObjectDTOS  = new ArrayList<>();
        List<BookObject> feedObjects = bookObjectRepository.findByBankBook_Id(bankBookId);
        for (BookObject feedObject : feedObjects) {
            BookObjectDTO BookObjectDTO = modelMapper.map(feedObject,BookObjectDTO.class);
            BookObjectDTOS.add(BookObjectDTO);
        }

        return BookObjectDTOS;
    }

    @Override
    public List<BookObjectDTO> getPagedFeedsByBookId(Long bankBookId, Pageable pageable) {
        List<BookObjectDTO> BookObjectDTOS  = new ArrayList<>();
        Page<BookObject> feedObjects = bookObjectRepository.findByBankBook_Id(bankBookId,pageable);
        for (BookObject feedObject : feedObjects.getContent()) {
            BookObjectDTO BookObjectDTO = modelMapper.map(feedObject,BookObjectDTO.class);
            BookObjectDTOS.add(BookObjectDTO);
        }
        return BookObjectDTOS;
    }

    @Override
    public List<BookObjectDTO> getAllFeeds() {
//        List<BookObject> feedObjects = bookObjectRepository.getAllBookObject();
//
//        for (BookObject feedObject : feedObjects) {
//            System.out.println(feedObject);
//        }
        return null;
    }

    @Override
    public BookObjectDTO addBookObject(BookObjectDTO BookObjectDTO) {

        BookObject feedObject = modelMapper.map(BookObjectDTO,BookObject.class);

        if(feedObject ==null || feedObject.getBankBook()==null){
            throw new SyncBaseException("Invalid FileObject passed");
        }

        BookObjectDTO check = this.getFeedByNameAndBookname(feedObject.getName(),feedObject.getBankBook().getBookName());

        if(check!=null){
            throw new SyncBaseException("BookObject Already Exists");
        }

        feedObject.setDelFlag("N");
        feedObject.setEnabled(true);
        feedObject = bookObjectRepository.save(feedObject);
        logger.info("File Object {}  has been Added",feedObject.getName());
        BookObjectDTO.setId(feedObject.getId());

        return BookObjectDTO;
    }

    @Override
    public BookObjectDTO getFeedByNameAndBookname(String ObjectName, String bookName) {
        System.out.println(ObjectName +" "+bookName);
        BankBookDTO bankBookDTO =  bookService.getBookByName(bookName);
        if(bankBookDTO==null){
            throw new SyncBaseException("Book Not found :: "+bookName);
        }
        System.out.println(bankBookDTO);
        BookObject feedObject = bookObjectRepository.findByNameAndBankBook_BookName(ObjectName,bankBookDTO.getBookName());
        BookObjectDTO BookObjectDTO = null;
        if(feedObject != null){
            BookObjectDTO = modelMapper.map(feedObject,BookObjectDTO.class);
        }
        return BookObjectDTO;
    }

    @Override
    public BookObjectDTO getBookObjectById(Long feedObjectId) {
        BookObject feedObject = bookObjectRepository.findById(feedObjectId).orElse(null);
        BookObjectDTO BookObjectDTO = null;
        if(feedObject != null){
            BookObjectDTO = modelMapper.map(feedObject,BookObjectDTO.class);
        }
        return BookObjectDTO;
    }



    @Override
    public BookObjectDTO updateBookObject(BookObjectDTO bookObjectDTO) {

        if(bookObjectDTO == null || bookObjectDTO.getBankBookDTO()== null){
            throw new SyncBaseException("Invalid FileObject DTO");
        }
        BankBookDTO bankBookDTO =  bookService.getBookByName(bookObjectDTO.getBankBookDTO().getBookName());
        if(bankBookDTO==null){
            throw new SyncBaseException("File Object Not found :: "+bookObjectDTO.getBankBookDTO().getBookName());
        }

        BookObject feedObject = bookObjectRepository.findByNameAndBankBook_BookName(bookObjectDTO.getName(),bookObjectDTO.getBankBookDTO().getBookName());

        if(feedObject==null){
            throw new SyncBaseException("File Object Does not Exist");
        }

        feedObject.setDefaultValue(bookObjectDTO.getDefaultValue());
        feedObject.setDescription(bookObjectDTO.getDescription());
        feedObject.setMappedColumn(bookObjectDTO.getMappedColumn());
        feedObject.setEnabled(bookObjectDTO.getEnabled());
        feedObject.setMappedColumnType(bookObjectDTO.getMappedColumnType());

        bookObjectRepository.save(feedObject);
        logger.info("File Object {} has been updated",feedObject.getName());

        return bookObjectDTO;
    }

    @Override
    public String uploadBookObjects(MultipartFile multipartFile,Long bookId) {
        String fileName = multipartFile.getOriginalFilename();


        File file = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);

        System.out.println(file.getPath());
        try {
            multipartFile.transferTo(file);

            CSVReader reader = new CSVReader(new FileReader(file));
            String[] line;
            long cnt = 0;
            while ((line = reader.readNext()) != null) {
                if(cnt==0){
                    List<String> csvHeader = Arrays.asList(configureHeaders);
                    if(!csvHeader.equals(Arrays.asList(line))){
                        throw new SyncBaseException("Invalid Header Found in Upload");
                    }
                }else{
                    List<String> datas = Arrays.asList(line);
                    BookObjectDTO feedObjectDTO = new BookObjectDTO();

                    BankBookDTO bankBookDTO = bookService.getBookByName(datas.get(0));
                    if(bankBookDTO == null){
                        throw new SyncBaseException("Invalid Book Name at row : "+cnt);
                    }
                    feedObjectDTO.setBankBookDTO(bankBookDTO);
                    feedObjectDTO.setMappedColumnType(datas.get(4));
                    feedObjectDTO.setName(datas.get(1));
                    feedObjectDTO.setDescription(datas.get(2));
                    feedObjectDTO.setDefaultValue(datas.get(5));
                    feedObjectDTO.setMappedColumn(datas.get(3));
                    feedObjectDTO.setDelFlag("N");
                    feedObjectDTO.setEnabled(true);

                    BookObjectDTO check = getFeedByNameAndBookname(feedObjectDTO.getName(),feedObjectDTO.getBankBookDTO().getBookName());
                    if(check == null){
                        addBookObject(feedObjectDTO);
                    }else{
                        updateBookObject(feedObjectDTO);
                    }
                }
                cnt++;
            }

            reader.close();

        } catch (IOException e) {
            file.delete();
            e.printStackTrace();
            throw new SyncBaseException(e.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            throw new SyncBaseException(e.getMessage());
        } finally{
            try{
                file.delete();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public void deleteBookObject(Long id) {
        BookObject bookObject = bookObjectRepository.findById(id).orElse(null);
        if(bookObject == null){
            throw new SyncBaseException("Book Not Found");
        }

        bookObject.setDelFlag("Y");
        bookObject.setDeletedOn(new Date());
        bookObjectRepository.save(bookObject);
    }
}
