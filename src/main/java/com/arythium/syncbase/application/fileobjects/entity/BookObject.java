package com.arythium.syncbase.application.fileobjects.entity;//package com.arythium.syncbase.aaplication.books.entity;

import com.arythium.syncbase.application.bankfiles.entity.BankBook;
import com.arythium.syncbase.core.entity.AbstractEntity;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@Table(name = "bank_book_object")
@Where(clause = "del_flag='N'")
public class BookObject extends AbstractEntity {

    private String name;
    private String description;

    @Column(name = "mapped_column")
    private String mappedColumn;

    @Column(name = "mapped_column_type")
    private String mappedColumnType;

    @Column(name = "default_value")
    private String defaultValue;

    private Boolean enabled;

    @ManyToOne()
    private BankBook bankBook;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BookObject that = (BookObject) o;
        return name.equals(that.name) &&
                bankBook.equals(that.bankBook);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode() + bankBook.hashCode();
        return result;
    }


}
