package com.arythium.syncbase.application.fileobjects.repository;

import com.arythium.syncbase.application.fileobjects.entity.BookObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//import com.arythium.syncbase.core.repository.AppCommonRepository;

/**
 * @author Timmy
 * @date 11/6/2018
 */

@Repository
public interface BookObjectRepository extends JpaRepository<BookObject, Long> {

//    BookObject findByNameAndBankBook_BookName(String name,String bankBookName);

    BookObject findByNameAndBankBook_BookName(String name,String bankBookName);

    List<BookObject> findByBankBook_BookName(String bankBookName);

    List<BookObject> findByBankBook_Id(Long bankBookId);

    Page<BookObject> findByBankBook_Id(Long bankBookId, Pageable pageable);


}
