package com.arythium.syncbase.application.fileobjects.dto;//package com.arythium.syncbase.aaplication.books.entity;

import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.core.dto.AbstractDto;
import lombok.Data;

@Data
public class BookObjectDTO extends AbstractDto {

    private String name;
    private String description;
    private String mappedColumn;
    private String mappedColumnType;
    private String defaultValue;
    private Boolean enabled;

    private BankBookDTO bankBookDTO;

}
