package com.arythium.syncbase.application.bankfiles.repository;

import com.arythium.syncbase.application.bankfiles.entity.BankBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Timmy
 * @date 11/6/2018
 */

@Repository
public interface BookRepository extends JpaRepository<BankBook, Long> {

    BankBook findByBookName(String bookName);

    List<BankBook> findByEnabledTrue();

}
