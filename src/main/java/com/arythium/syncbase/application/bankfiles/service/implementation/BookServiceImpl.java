package com.arythium.syncbase.application.bankfiles.service.implementation;

import com.arythium.syncbase.application.filesync.service.implementation.DBUtilServiceImpl;
import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.application.bankfiles.entity.BankBook;
import com.arythium.syncbase.application.bankfiles.repository.BookRepository;
import com.arythium.syncbase.application.bankfiles.service.BookService;
import com.arythium.syncbase.application.filesync.dto.BookSyncDto;
import com.arythium.syncbase.application.filesync.service.BookSyncService;
import com.arythium.syncbase.application.fileobjects.dto.BookObjectDTO;
import com.arythium.syncbase.application.fileobjects.service.BookObjectService;
import com.arythium.syncbase.core.code.service.CodeService;
import com.arythium.syncbase.core.exceptions.SyncBaseException;
import com.opencsv.CSVReader;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@Service
public class BookServiceImpl implements BookService {

    private static final Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    BookRepository bookRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    CodeService codeService;

    @Autowired
    DBUtilServiceImpl dbUtilService;

    @Autowired
    BookSyncService bookSyncService;

    @Autowired
    BookObjectService bookObjectService;

    @Value("${book.config.header_columns}")
    private String[] configureHeaders;

    @Override
    public List<BankBookDTO> getAllBooks() {
        List<BankBookDTO> bankBookDTOS = new ArrayList<>();

        bookRepository.findAll().forEach(bankBook -> {
            bankBookDTOS.add(modelMapper.map(bankBook, BankBookDTO.class));
        });
        return bankBookDTOS;
    }

    @Override
    public List<BankBookDTO> getUnFedBooks() {
        List<BankBookDTO> bankBookDTOS = new ArrayList<>();

        bookRepository.findByEnabledTrue().forEach(bankBook -> {
            BookSyncDto syncAudit = bookSyncService.getLatestSyncByBookName(bankBook.getBookName());
            if(syncAudit !=null){

                if(syncAudit.getStatus().equals("COMPLETED") || syncAudit.getStatus().equals("STOPPED") ){
                    bankBookDTOS.add(modelMapper.map(bankBook, BankBookDTO.class));
                }else{
                    logger.error("{} is currently {}, started at :{} ",bankBook.getBookName(),syncAudit.getStatus(),syncAudit.getDateCreated());
                }
            }else{
                bankBookDTOS.add(modelMapper.map(bankBook, BankBookDTO.class));
            }


        });

        return bankBookDTOS;
    }

    @Override
    public BankBookDTO getBookByName(String bookName) {
        BankBook book = bookRepository.findByBookName(bookName);
        if(book==null){
            return null;
        }
        BankBookDTO bankBookDTO = modelMapper.map(book, BankBookDTO.class);

        return bankBookDTO;
    }

    @Override
    public BankBookDTO getBookById(Long bookId) {
        BankBook book = bookRepository.findById(bookId).orElse(null);
        if(book==null){
            return null;
        }
        BankBookDTO bankBookDTO = modelMapper.map(book, BankBookDTO.class);

        return bankBookDTO;
    }


    @Override
    public BankBookDTO addBook(BankBookDTO bankBookDTO) {

        if(bankBookDTO == null){
            throw new SyncBaseException("Book Cannot be null");
        }

        BankBook check = bookRepository.findByBookName(bankBookDTO.getBookName());
        if(check != null){
            throw new SyncBaseException("File Already exist");
        }

        BankBook book =  modelMapper.map(bankBookDTO,BankBook.class);


        book.setFeedObjects(null);
        book.setDelFlag("N");
        book = bookRepository.save(book);
        logger.info("Book Added :: {} ",book.getBookName());
        bankBookDTO.setId(book.getId());
        return bankBookDTO;
    }

    @Override
    public BankBookDTO updateBook(BankBookDTO bankBookDTO) {

        BankBook check = bookRepository.findByBookName(bankBookDTO.getBookName());
        if(check == null){
            throw new SyncBaseException("File Does not exist");
        }
        check.setBookPrefix(bankBookDTO.getBookPrefix());
        check.setMappedTableName(bankBookDTO.getMappedTableName());
        check.setDescription(bankBookDTO.getDescription());
        check.setEnabled(bankBookDTO.getEnabled());

        check = bookRepository.save(check);

        logger.info("File Config {} has been Updated",check.getBookName());
        bankBookDTO.setId(check.getId());
        return bankBookDTO;
    }

    @Override
    public String uploadBooks(MultipartFile multipartFile) {


        System.out.println("conn :: "+configureHeaders);
        String fileName = multipartFile.getOriginalFilename();

        File file = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
        try {
            multipartFile.transferTo(file);

            CSVReader reader = new CSVReader(new FileReader(file));
            String[] line;
            long cnt = 0;
            while ((line = reader.readNext()) != null) {
                if(cnt==0){
                    List<String> csvHeader = Arrays.asList(configureHeaders);
                    if(!csvHeader.equals(Arrays.asList(line))){
                        throw new SyncBaseException("Invalid Header Found in Upload");
                    }
                }else{
                    List<String> datas = Arrays.asList(line);
                    BankBookDTO bankBookDTO = new BankBookDTO();
                    bankBookDTO.setBookName(datas.get(0));
                    bankBookDTO.setMappedTableName(datas.get(2));
                    bankBookDTO.setBookPrefix(datas.get(1));
                    bankBookDTO.setEnabled(true);
                    bankBookDTO.setDelFlag("N");

                    BankBookDTO check = getBookByName(bankBookDTO.getBookName());
                    if(check == null){
                        addBook(bankBookDTO);
                    }else{
                        updateBook(bankBookDTO);
                    }
                }
                cnt++;
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new SyncBaseException("Error Occurred configuring book "+e.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            throw new SyncBaseException("Error Occurred configuring book "+e.getMessage());
        }
        return null;
    }

    @Override
    public void deleteBook(Long id) {

        BankBook bankBook = bookRepository.findById(id).orElse(null);
        if(bankBook == null){
            throw new SyncBaseException("Book Not Found");
        }

        List<BookObjectDTO> bookObjectDTOS = bookObjectService.getFeedsByBookId(bankBook.getId());

        for(int i=0;i<bookObjectDTOS.size();i++){
            BookObjectDTO bookObjectDTO = bookObjectDTOS.get(i);
            bookObjectService.deleteBookObject(bookObjectDTO.getId());
        }
        bankBook.setDelFlag("Y");
        bankBook.setDeletedOn(new Date());
        bookRepository.save(bankBook);
    }



}
