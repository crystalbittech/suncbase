package com.arythium.syncbase.application.bankfiles.service;

import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BookService {

    List<BankBookDTO> getAllBooks();

    List<BankBookDTO> getUnFedBooks();

    BankBookDTO getBookByName(String bookName);

    BankBookDTO getBookById(Long bookId);

    BankBookDTO addBook(BankBookDTO bankBookDTO);

    BankBookDTO updateBook(BankBookDTO bankBookDTO);

    String uploadBooks(MultipartFile file);

    void deleteBook(Long id);



}
