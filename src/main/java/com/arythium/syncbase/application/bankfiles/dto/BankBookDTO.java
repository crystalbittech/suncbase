package com.arythium.syncbase.application.bankfiles.dto;

import com.arythium.syncbase.core.dto.AbstractDto;
import lombok.Data;

@Data
public class BankBookDTO extends AbstractDto {

    private String bookName;
    private String description;
    private String bookPrefix;
    private String mappedTableName;
    private Boolean enabled;

}
