package com.arythium.syncbase.application.bankfiles.dto;

public enum BookSyncStatus {
    STARTED,
    PROCESSING,
    PAUSED,
    COMPLETED,
    STOPPED
}
