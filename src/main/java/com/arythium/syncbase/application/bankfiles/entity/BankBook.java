package com.arythium.syncbase.application.bankfiles.entity;

import com.arythium.syncbase.application.fileobjects.entity.BookObject;
import com.arythium.syncbase.core.entity.AbstractEntity;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "bank_book")
@Where(clause = "del_flag='N'")
public class BankBook extends AbstractEntity {

    @Column(name = "book_name")
    private String bookName;

    @Column(name = "book_prefix")
    private String bookPrefix;

    @Column(name = "mapped_table_name")
    private String mappedTableName;

    private String description;

    private Boolean enabled;

    @Column(name = "book_name")
    @OneToMany(mappedBy = "bankBook",fetch = FetchType.EAGER)
    private List<BookObject> feedObjects;


}
