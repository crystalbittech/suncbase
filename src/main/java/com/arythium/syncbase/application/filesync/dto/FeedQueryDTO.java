package com.arythium.syncbase.application.filesync.dto;//package com.arythium.syncbase.aaplication.books.entity;

import lombok.Data;

import java.util.ArrayList;

@Data
public class FeedQueryDTO {

    private String queryString;
    private int numOfValues;
    private ArrayList<Integer> ignoredCells;

}
