package com.arythium.syncbase.application.filesync.service;

import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;

import java.util.List;

public interface HandShakeService {

    List<String> getPendingFiles(String pattern);

    void generateFeed(String FileName, BankBookDTO bankBookDTO);

    void scheduledFeedGeneration();

}
