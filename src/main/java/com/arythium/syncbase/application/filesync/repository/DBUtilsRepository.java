package com.arythium.syncbase.application.filesync.repository;

import com.arythium.syncbase.application.filesync.dto.FeedQueryDTO;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class DBUtilsRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional
	public void insertWithQuery(String[] queryValues, FeedQueryDTO feedQueryDTO) {
		Query query = entityManager.createNativeQuery(feedQueryDTO.getQueryString());
		for(int a=0;a<feedQueryDTO.getNumOfValues();a++){
			query.setParameter(a+1, queryValues[a]);
		}
		query.executeUpdate();
	}

}
