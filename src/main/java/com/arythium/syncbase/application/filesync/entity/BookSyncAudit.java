package com.arythium.syncbase.application.filesync.entity;//package com.arythium.syncbase.aaplication.books.entity;

import com.arythium.syncbase.core.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "book_sync_audit")
public class BookSyncAudit extends AbstractEntity {

    @ManyToOne
    private BookSync bookSync;

    @Column(name = "initiated_by")
    private String initiatedBy;

    @Column(name = "sync_status")
    private String syncStatus;


}
