package com.arythium.syncbase.application.filesync.service.implementation;

import com.arythium.syncbase.application.filesync.repository.DBUtilsRepository;
import com.arythium.syncbase.application.filesync.service.DBUtilService;
import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.application.filesync.dto.FeedQueryDTO;
import com.arythium.syncbase.application.fileobjects.dto.BookObjectDTO;
import com.arythium.syncbase.application.fileobjects.service.BookObjectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class DBUtilServiceImpl implements DBUtilService {

    @Autowired
    DBUtilsRepository dbUtilsRepository;

    @Autowired
    BookObjectService bookObjectService;

    @Override
    public FeedQueryDTO generateQuery(String[] csvHeaders, BankBookDTO bookDTO) {

        FeedQueryDTO feedQueryDTO = new FeedQueryDTO();
        ArrayList<Integer> cellsToIgnore = new ArrayList<>();
        String objectTable = bookDTO.getMappedTableName();
        String finalQuery = "";
        int columnLength = csvHeaders.length;

        for(int i=0;i<csvHeaders.length;i++){

            String columnHeader = csvHeaders[i];

            BookObjectDTO feedObjectDTO = bookObjectService.getFeedByNameAndBookname(columnHeader,bookDTO.getBookName());
            if(feedObjectDTO == null){
                //skip this guy and make sure values down are skipped also
                columnLength = columnLength -1;
                cellsToIgnore.add(i);
                if(i == csvHeaders.length -1 ){
                    finalQuery  = finalQuery + ") values ("+ StringUtils.repeat("?","," ,columnLength )+"}";
                }
                continue;
            }else{
                columnHeader = feedObjectDTO.getMappedColumn();
                if(i==0){
                    finalQuery  = finalQuery + "insert into "+objectTable+"("+columnHeader;
                }else if(i == csvHeaders.length -1 ){
                    finalQuery  = finalQuery + ","+columnHeader+") values ("+ StringUtils.repeat("?","," ,columnLength )+")";
                }else{
                    finalQuery  = finalQuery + ","+columnHeader;
                }
            }
        }

        feedQueryDTO.setIgnoredCells(cellsToIgnore);
        feedQueryDTO.setQueryString(finalQuery);
        feedQueryDTO.setNumOfValues(columnLength);
        return feedQueryDTO;
    }

    @Override
    public void saveRecord(String[] csvRows, FeedQueryDTO feedQueryDTO) {
        dbUtilsRepository.insertWithQuery(csvRows,feedQueryDTO);
    }
}
