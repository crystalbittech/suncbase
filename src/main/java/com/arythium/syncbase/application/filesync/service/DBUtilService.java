package com.arythium.syncbase.application.filesync.service;

import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.application.filesync.dto.FeedQueryDTO;

public interface DBUtilService {

    FeedQueryDTO generateQuery(String[] csvHeaders, BankBookDTO bookDTO);

    void saveRecord(String[] csvHeaders, FeedQueryDTO feedQueryDTO);
}
