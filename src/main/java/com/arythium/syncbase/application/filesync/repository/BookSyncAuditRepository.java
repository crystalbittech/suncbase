package com.arythium.syncbase.application.filesync.repository;

import com.arythium.syncbase.application.filesync.entity.BookSyncAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Timmy
 * @date 11/6/2018
 */

@Repository
public interface BookSyncAuditRepository extends JpaRepository<BookSyncAudit, Long> {

    List<BookSyncAudit> findByBookSync_Id(Long bookSyncId);
}
