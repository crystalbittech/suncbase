package com.arythium.syncbase.application.filesync.dto;//package com.arythium.syncbase.aaplication.books.entity;

import com.arythium.syncbase.core.dto.AbstractDto;
import lombok.Data;

import java.util.Date;

@Data
public class BookSyncDto extends AbstractDto {

    
    private String bookName;

    private String processId;

    private String status;

    private Date completedOn;

    private Date startedOn;

    private Long lastCount;

    private String message;



}
