package com.arythium.syncbase.application.filesync.service.implementation;

import com.arythium.syncbase.application.filesync.dto.BookSyncDto;
import com.arythium.syncbase.application.filesync.dto.BookSyncDashBoardAudit;
import com.arythium.syncbase.application.filesync.dto.BookSyncStatus;
import com.arythium.syncbase.application.filesync.entity.BookSync;
import com.arythium.syncbase.application.filesync.entity.BookSyncAudit;
import com.arythium.syncbase.application.filesync.repository.BookSyncAuditRepository;
import com.arythium.syncbase.application.filesync.repository.BookSyncRepository;
import com.arythium.syncbase.application.filesync.service.BookSyncService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class BookSyncServiceImpl implements BookSyncService {

    private static final Logger logger = LoggerFactory.getLogger(BookSyncServiceImpl.class);


    @Autowired
    BookSyncRepository bookSyncRepository;

    @Autowired
    BookSyncAuditRepository bookSyncAuditRepository;

    @Autowired
    ModelMapper modelMapper;


    @Override
    public BookSyncDto addBookSync(BookSyncDto bookSyncStatus) {
        BookSync bookSync = null;
        if( bookSyncStatus.getStatus().equals("STARTED") ){
            bookSync = new BookSync();
            bookSync.setBookName(bookSyncStatus.getBookName());
        }else{
            bookSync = bookSyncRepository.findFirstByBookNameOrderByDateCreatedDesc(bookSyncStatus.getBookName());
            if( bookSyncStatus.getStatus().equals("COMPLETED") ){
                bookSync.setCompletedOn(new Date());
            }
            bookSync.setLastCount(bookSyncStatus.getLastCount());
            bookSync.setMessage(bookSyncStatus.getMessage());
        }
        bookSync.setProcessId("PR"+new Date().getTime());
        bookSync.setStatus(BookSyncStatus.valueOf(bookSyncStatus.getStatus()));
        bookSync = bookSyncRepository.save(bookSync);
        bookSyncStatus.setId(bookSync.getId());

        BookSyncAudit bookSyncAudit = modelMapper.map(bookSync,BookSyncAudit.class);
        bookSyncAudit.setId(null);
        bookSyncAudit.setDateCreated(new Date());
        bookSyncAudit.setBookSync(bookSync);
        bookSyncAudit.setSyncStatus(bookSyncStatus.getStatus());
        bookSyncAudit.setInitiatedBy("Admin1");
        bookSyncAuditRepository.save(bookSyncAudit);

        return bookSyncStatus;
    }

    @Override
    public BookSyncDto getLatestSyncByBookName(String bookName) {
        BookSync bookSync = bookSyncRepository.findFirstByBookNameOrderByDateCreatedDesc(bookName);
        if(bookSync ==null){
            return null;
        }
        return modelMapper.map(bookSync, BookSyncDto.class);
    }

    @Override
    public BookSyncDto getBookSyncByProcID(String procID) {

        BookSync bookSync = bookSyncRepository.findByProcessId(procID);
        if(bookSync ==null){
            return null;
        }
        return modelMapper.map(bookSync, BookSyncDto.class);
    }

    @Override
    public boolean isSyncPaused(BookSyncDto bookSyncDto) {
        BookSync syncAudit = bookSyncRepository.findFirstByBookNameOrderByDateCreatedDesc(bookSyncDto.getBookName());
        if(syncAudit !=null && syncAudit.getStatus().equals(BookSyncStatus.PAUSED)){
            return true;
        }
        return false;
    }

    @Override
    public List<BookSyncDto> getSyncAudit() {
        List<BookSync> bookSyncs = bookSyncRepository.findAllByOrderByDateCreatedDesc();
        List<BookSyncDto> bookSyncDtos = new ArrayList<>();
        bookSyncs.forEach(bookSync -> {
            BookSyncDto bookSyncDto = modelMapper.map(bookSync, BookSyncDto.class);
            bookSyncDtos.add(bookSyncDto);
        });
        return bookSyncDtos;
    }

    @Override
    public BookSyncDashBoardAudit getSyncDashBoardData() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String todaysDate = simpleDateFormat.format(new Date());
        Date todaysStartTime = null;
        try {
            todaysStartTime = simpleDateFormat.parse(todaysDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        BookSyncDashBoardAudit bookSyncDashBoardAudit = new BookSyncDashBoardAudit();


        int cnt1 = bookSyncRepository.countByStatusAndStartedOnGreaterThan(BookSyncStatus.COMPLETED,todaysStartTime);

        bookSyncDashBoardAudit.setCOMPLETED_BOOKS_COUNT(String.valueOf(cnt1));

        int cnt2 = bookSyncRepository.countByStatusAndStartedOnGreaterThan(BookSyncStatus.PROCESSING,todaysStartTime);
        bookSyncDashBoardAudit.setPROCESSED_BOOKS_COUNT(String.valueOf(cnt2));

        int cnt3 = bookSyncRepository.countByStatusAndStartedOnGreaterThan(BookSyncStatus.PAUSED,todaysStartTime);
        bookSyncDashBoardAudit.setPAUSED_BOOKS_COUNT(String.valueOf(cnt3));

        int cnt4 = bookSyncRepository.countByStatusAndStartedOnGreaterThan(BookSyncStatus.STOPPED,todaysStartTime);
        bookSyncDashBoardAudit.setSTOPPED_BOOKS_COUNT(String.valueOf(cnt4));

        return bookSyncDashBoardAudit;
    }


}
