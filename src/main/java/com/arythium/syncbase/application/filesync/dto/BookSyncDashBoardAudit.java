package com.arythium.syncbase.application.filesync.dto;//package com.arythium.syncbase.aaplication.books.entity;

import com.arythium.syncbase.core.dto.AbstractDto;
import lombok.Data;

@Data
public class BookSyncDashBoardAudit extends AbstractDto {

    
    private String PROCESSED_BOOKS_COUNT;
    private String COMPLETED_BOOKS_COUNT;
    private String PAUSED_BOOKS_COUNT;
    private String STOPPED_BOOKS_COUNT;



}
