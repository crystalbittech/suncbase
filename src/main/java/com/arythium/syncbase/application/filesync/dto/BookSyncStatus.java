package com.arythium.syncbase.application.filesync.dto;

public enum BookSyncStatus {
    STARTED,
    PROCESSING,
    PAUSED,
    COMPLETED,
    STOPPED
}
