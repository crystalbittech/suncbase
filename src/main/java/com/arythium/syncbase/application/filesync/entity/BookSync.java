package com.arythium.syncbase.application.filesync.entity;//package com.arythium.syncbase.aaplication.books.entity;

import com.arythium.syncbase.application.filesync.dto.BookSyncStatus;
import com.arythium.syncbase.core.entity.AbstractEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity

@Table(name = "book_sync")
public class BookSync extends AbstractEntity {

    @Column(name = "process_id")
    private String processId;

    @Column(name = "book_name")
    private String bookName;

    @Enumerated(EnumType.STRING)
    private BookSyncStatus status;

    @Column(name = "completed_on")
    private Date completedOn;

    @Column(name = "started_on")
    private Date startedOn = dateCreated;

    @Column(name = "last_count")
    private Long lastCount;

    private String message;



}
