package com.arythium.syncbase.application.filesync.entity;

import lombok.Data;

import java.util.ArrayList;

@Data
public class GenericEntityValues {

    ArrayList<String> stringArrayList;
}
