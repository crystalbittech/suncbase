package com.arythium.syncbase.application.filesync.service.implementation;

import com.arythium.syncbase.application.filesync.service.DBUtilService;
import com.arythium.syncbase.application.filesync.service.HandShakeService;
import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.application.bankfiles.dto.BookSyncStatus;
import com.arythium.syncbase.application.bankfiles.service.BookService;
import com.arythium.syncbase.application.filesync.dto.FeedQueryDTO;
import com.arythium.syncbase.application.filesync.dto.BookSyncDto;
import com.arythium.syncbase.application.filesync.service.BookSyncService;
import com.arythium.syncbase.core.exceptions.SyncBaseException;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class HandShakeServiceImpl implements HandShakeService {

    private static final Logger logger = LoggerFactory.getLogger(HandShakeServiceImpl.class);

    @Value("${syncbase.files.directory}")
    private String fileDirectory;

    @Value("${syncbase.files.search_pattern}")
    private String searchPattern;


    @Autowired
    BookService bookService;

    @Autowired
    BookSyncService bookSyncService;

    @Autowired
    DBUtilService dbUtilService;


    @Override
    public List<String> getPendingFiles(String filePrefix) {
        List<String> pendingFiles = new ArrayList<>();

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();

        File folder =  new File(fileDirectory);
        if(!folder.exists()){
            folder = new File(classLoader.getResource("").getFile());
        }

        File[] folderItems = folder.listFiles();
        for (final File f : folderItems) {

            if (f.isFile() && f.getName().matches(filePrefix + searchPattern)) {
                pendingFiles.add(f.getPath());
            }

        }
        return pendingFiles;
    }

    @Override
    public void generateFeed(String fileName,BankBookDTO bankBookDTO) {

            long cnt = 0;

            BookSyncDto bookSyncDto = new BookSyncDto();
            bookSyncDto.setBookName(bankBookDTO.getBookName());
            bookSyncDto.setStatus(String.valueOf(BookSyncStatus.STARTED));
            bookSyncDto.setLastCount(cnt);
            bookSyncService.addBookSync(bookSyncDto);

            logger.info("Data sync started using :: {}",fileName);


            File file = new File(fileName);

            FeedQueryDTO feedQueryDTO= null;

            CSVReader reader = null;
            boolean containsHeader = true;
            try {

                bookSyncDto.setStatus(String.valueOf(BookSyncStatus.PROCESSING));
                bookSyncDto.setLastCount(cnt);
                bookSyncService.addBookSync(bookSyncDto);

                reader = new CSVReader(new FileReader(file));
                String[] line;

                while ((line = reader.readNext()) != null) {
                    if(containsHeader && cnt==0){
                        feedQueryDTO = dbUtilService.generateQuery(line,bankBookDTO);
                        logger.info("Query Properties {} ",feedQueryDTO);
                        if(feedQueryDTO.getIgnoredCells().size() > 0){
                            throw new SyncBaseException("Invalid Column Headers Found");
                        }
                    }else{
                        List<String> datas = Arrays.asList(line);
                        logger.info("{}. {}",cnt,feedQueryDTO.getQueryString());
                        try{
                            dbUtilService.saveRecord(line,feedQueryDTO);
                        }catch(Exception e){
                            e.printStackTrace();
                            throw new SyncBaseException("Error occurred inserting record at row: "+cnt + " Cause : "+e.getCause().getCause());
                        }
                    }

                    boolean isPaused = bookSyncService.isSyncPaused(bookSyncDto);
                    if(isPaused){
                        logger.error("Sync was paused at {} ",cnt);
                        break;
                    }

                    cnt++;
                }

                reader.close();

                logger.info("Data sync ended using :: {}",fileName);

                Path source = Paths.get(fileName);
                String backoutFileName = fileName+".success";
                Files.move(source, source.resolveSibling(backoutFileName));
                logger.info("{} file has been backed out to {}",fileName,backoutFileName);

                bookSyncDto.setStatus(String.valueOf(BookSyncStatus.COMPLETED));
                bookSyncDto.setLastCount(cnt);
                bookSyncService.addBookSync(bookSyncDto);



            } catch (IOException e) {
                e.printStackTrace();
                bookSyncDto.setStatus(String.valueOf(BookSyncStatus.STOPPED));
                bookSyncDto.setLastCount(cnt);
                bookSyncDto.setMessage(e.getMessage());
                bookSyncService.addBookSync(bookSyncDto);
                throw new SyncBaseException("Error occurred inserting record ");
            } catch (Exception e){
                e.printStackTrace();
                bookSyncDto.setStatus(String.valueOf(BookSyncStatus.STOPPED));
                bookSyncDto.setLastCount(cnt);
                bookSyncDto.setMessage(e.getMessage());
                bookSyncService.addBookSync(bookSyncDto);
                throw new SyncBaseException("Error occurred inserting record ");
            }
            logger.info("");
    }


    @Override
    @Scheduled(fixedRateString = "${fixedRate.in.milliseconds}")
    public void scheduledFeedGeneration() {
        logger.info("**** Scheduled JOB for data handshake has begun ***");
        List<BankBookDTO> bankBookDTOS = bookService.getUnFedBooks();
        logger.info("Number of books to process :: {}",bankBookDTOS.size());
        for(BankBookDTO bankBookDTO : bankBookDTOS){

            logger.info("[ Generating feeds for  {} ]",bankBookDTO.getBookName());
            List<String> csvFiles = this.getPendingFiles(bankBookDTO.getBookPrefix());
            if(csvFiles.size() > 0) {
                this.generateFeed(csvFiles.get(0),bankBookDTO);
            }else{
                logger.info("[ No File seen for {} data sync ]",bankBookDTO.getBookName());
            }
        }
    }
}
