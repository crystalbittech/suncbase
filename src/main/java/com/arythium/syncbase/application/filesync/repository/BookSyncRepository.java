package com.arythium.syncbase.application.filesync.repository;

import com.arythium.syncbase.application.filesync.dto.BookSyncStatus;
import com.arythium.syncbase.application.filesync.entity.BookSync;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

//import com.arythium.syncbase.core.repository.AppCommonRepository;

/**
 * @author Timmy
 * @date 11/6/2018
 */

@Repository
public interface BookSyncRepository extends JpaRepository<BookSync, Long> {

    BookSync findFirstByBookNameOrderByDateCreatedDesc(String bookName);

    BookSync findByProcessId(String procID);

    List<BookSync> findAllByOrderByDateCreatedDesc();

    int countByStatusAndStartedOnGreaterThan(BookSyncStatus syncStatus, Date completedOn);
}
