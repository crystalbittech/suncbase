package com.arythium.syncbase.application.filesync.repository;//package com.arythium.syncbase.application.DBUtil.repository.impl;
//
//import com.arythium.syncbase.application.DBUtil.repository.DBUtilsRepositoryCustom;
//import com.arythium.syncbase.application.feeds.dto.FeedQueryDTO;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
//
//
//public class DBUtilsRepositoryImpl implements DBUtilsRepositoryCustom {
//
//    @PersistenceContext
//    private EntityManager entityManager;
//
//    @Transactional
//    public void insertWithQuery(String[] queryValues, FeedQueryDTO feedQueryDTO) {
//        Query query = entityManager.createNativeQuery(feedQueryDTO.getQueryString());
//        for(int a=0;a<feedQueryDTO.getNumOfValues();a++){
//            query.setParameter(a+1, queryValues[a]);
//        }
//        query.executeUpdate();
//    }
//}
