package com.arythium;

import com.arythium.syncbase.application.bankfiles.dto.BankBookDTO;
import com.arythium.syncbase.application.bankfiles.service.BookService;
import com.arythium.syncbase.application.fileobjects.dto.BookObjectDTO;
import com.arythium.syncbase.application.fileobjects.service.BookObjectService;
import com.arythium.syncbase.core.exceptions.SyncBaseException;
import com.arythium.syncbase.core.usermgt.model.SystemUser;
import com.arythium.syncbase.core.usermgt.repository.ApplicationUserRepository;
import com.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
//public class Application extends SpringBootServletInitializer {
public class Application  {

	@Value("${syncbase.files.directory}")
	private String fileDirectory;

	@Value("${syncbase.files.config_file1}")
	private String configFile1;

	@Value("${syncbase.files.config_file2}")
	private String configFile2;


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
//		return app.sources(Application.class);
//	}

	@Bean
	public CommandLineRunner demo(BookService bookService, BookObjectService feedsService,
								  ApplicationUserRepository applicationUserRepository, PasswordEncoder passwordEncoder) {
		return (args) -> {

			configureBankBooks(configFile1,bookService);

			configureBankBookObjects(configFile2,bookService,feedsService);

			configureSystemUser(applicationUserRepository,passwordEncoder);
		};
	}


	private void configureBankBooks(String fileName,BookService bookService){


		File file = getFileFromPath(fileName);

		CSVReader reader;

		try {
			reader = new CSVReader(new FileReader(file));
			String[] line;
			long cnt = 0;
			while ((line = reader.readNext()) != null) {
				if(cnt==0){

				}else{
					List<String> datas = Arrays.asList(line);
					BankBookDTO bankBookDTO = new BankBookDTO();
					bankBookDTO.setBookName(datas.get(0));
					bankBookDTO.setBookPrefix(datas.get(1));
					bankBookDTO.setMappedTableName(datas.get(2));
					bankBookDTO.setEnabled(true);

					BankBookDTO check = bookService.getBookByName(bankBookDTO.getBookName());
					if(check == null){
						bookService.addBook(bankBookDTO);
					}else{
						bookService.updateBook(bankBookDTO);
					}
					System.out.println(datas);
				}
				cnt++;
			}

			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e){
            e.printStackTrace();
            throw new SyncBaseException("Error Occurred configuring book "+e.getMessage());
        }


    }

	private void configureSystemUser(ApplicationUserRepository userRepository,PasswordEncoder passwordEncoder){
		String adminName = "admin";
		SystemUser systemUser = userRepository.findByUserName(adminName);
		if(systemUser==null){
			//create User
			systemUser = new SystemUser();
			systemUser.setEmail("admin@testmail.com");
			systemUser.setFirstName("admin");
			systemUser.setLastName("user");
			systemUser.setUserName(adminName);
			systemUser.setPassword(passwordEncoder.encode("password@123"));

			userRepository.save(systemUser);
		}


    }

	private void configureBankBookObjects(String fileName,BookService bookService,BookObjectService feedsService){


		File file =  getFileFromPath(fileName);

		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(file));
			String[] line;
			long cnt = 0;
			while ((line = reader.readNext()) != null) {
				if(cnt==0){
				}else{
					List<String> datas = Arrays.asList(line);
					BookObjectDTO feedObjectDTO = new BookObjectDTO();
					BankBookDTO bankBookDTO = bookService.getBookByName(datas.get(0));

					if(bankBookDTO == null){
						throw new SyncBaseException("Invalid Book Name at row : "+cnt);
					}
					feedObjectDTO.setBankBookDTO(bankBookDTO);
					feedObjectDTO.setName(datas.get(1));
					feedObjectDTO.setDescription(datas.get(2));
					feedObjectDTO.setMappedColumn(datas.get(3));
					feedObjectDTO.setMappedColumnType(datas.get(4));
					feedObjectDTO.setDefaultValue(datas.get(5));

					BookObjectDTO check = feedsService.getFeedByNameAndBookname(feedObjectDTO.getName(),feedObjectDTO.getBankBookDTO().getBookName());
					if(check == null){
						feedsService.addBookObject(feedObjectDTO);
					}else{
						feedsService.updateBookObject(feedObjectDTO);
					}
				}
				cnt++;
			}

			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
			throw new SyncBaseException("Error Occurred configuring book objects"+e.getMessage());
		} catch (Exception e){
		    e.printStackTrace();
            throw new SyncBaseException("Error Occurred configuring book objects"+e.getMessage());
        }

	}


	private File getFileFromPath(String fileName){


		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        System.out.println("here is :: "+classLoader.getResource(""));
		File file =  new File(fileDirectory + fileName);
		if(!file.exists()){
            System.out.println("file doesnt exist "+file);
			File folder = new File(classLoader.getResource("").getFile());

			File[] folderItems = folder.listFiles();
			for (final File f : folderItems) {
				if (f.isFile() && f.getName().matches(fileName)) {
					file = f;
				}
			}
		}
		return file;
	}

}
